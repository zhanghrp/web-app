package com.alipay.sofa.config;

import com.alipay.drm.client.DRMClient;
import com.alipay.drm.client.api.annotation.DAttribute;
import com.alipay.drm.client.api.annotation.DObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author sunqing
 * @date 2020/10/26 10:09
 */
@Component
@DObject(region = "antCloud",appName = "core-app-server",id = "com.alipay.sofa.config.DynamicConfig")
public class DynamicConfig {

    private final static Logger LOGGER = LoggerFactory.getLogger(DynamicConfig.class);

    @DAttribute
    private String name ="张三";

    @DAttribute
    private int age;

    @DAttribute
    private boolean man;

    @PostConstruct
    public void registe(){
        DRMClient.getInstance().register(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        LOGGER.info("name is to be changed to {}",name);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isMan() {
        return man;
    }

    public void setMan(boolean man) {
        this.man = man;
    }
}
