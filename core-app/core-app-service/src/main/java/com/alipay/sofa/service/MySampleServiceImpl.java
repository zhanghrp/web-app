package com.alipay.sofa.service;

import com.alipay.sofa.config.DynamicConfig;
import com.alipay.sofa.facade.MySampleService;
import com.alipay.sofa.runtime.api.annotation.SofaService;
import com.alipay.sofa.runtime.api.annotation.SofaServiceBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author 浩鲸新智能
 */
@Service
@SofaService(interfaceType=MySampleService.class,bindings = @SofaServiceBinding(bindingType = "bolt"))
public class MySampleServiceImpl implements MySampleService, ApplicationContextAware {

    private final static Logger LOGGER = LoggerFactory.getLogger(MySampleServiceImpl.class);

    private int count;

    @Autowired
    private DynamicConfig dynamicConfig;

    private ApplicationContext applicationContext;

    @Override
    public String hello() {
        return "hello SOFARpc "+count++;
    }

    @Override
    public String getDynamicName() {
        return dynamicConfig.getName();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }
}
