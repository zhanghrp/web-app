package com.alipay.sofa.facade;

/**
 * @author 浩鲸新智能
 */
public interface MySampleService {
    /**
     * 简单服务发布模拟
     * @return 打招呼hello次数
     */
    String hello();

    /**
     * 获取动态配置名称
     * @return 动态配置名称
     */
    String getDynamicName();

}
