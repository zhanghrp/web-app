package com.alipay.sofa.endpoint.impl;

import com.alipay.sofa.facade.MySampleService;
import com.alipay.sofa.runtime.api.annotation.SofaReference;
import com.alipay.sofa.runtime.api.annotation.SofaReferenceBinding;
import org.springframework.stereotype.Component;

/**
 * @author 浩鲸新智能
 */
@Component
public class ReferenceHandler {

    @SofaReference(interfaceType = MySampleService.class,binding = @SofaReferenceBinding(bindingType = "bolt"))
    private MySampleService mySampleService;

    public MySampleService getMySampleService() {
        return mySampleService;
    }

    public void setMySampleService(MySampleService mySampleService) {
        this.mySampleService = mySampleService;
    }
}
