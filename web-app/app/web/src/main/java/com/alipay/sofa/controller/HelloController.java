package com.alipay.sofa.controller;

import com.alipay.sofa.endpoint.impl.ReferenceHandler;
import com.alipay.sofa.facade.MySampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 浩鲸新智能
 */
@RestController
public class HelloController {

    @Autowired
    private ReferenceHandler referenceHandler;

    @RequestMapping("hello")
    public String hello(){
        MySampleService sampleService =referenceHandler.getMySampleService();
        return sampleService.hello();
    }

    @RequestMapping("getName")
    public String getDynamicName(){
        MySampleService sampleService =referenceHandler.getMySampleService();
        return sampleService.getDynamicName();
    }
}
